package kz.greetgo.test.genrate.iin;

import kz.greetgo.test.genrate.iin.generator.CheckIIN;
import kz.greetgo.test.genrate.iin.generator.GeneratorIIN;

/**
 * Created by Erlan Ibraev on 31.03.2017.
 */
public class Main {

    public static void main(String... args) {
        GeneratorIIN generatorIIN = new GeneratorIIN();
        int i=0;
        while( i < 10) {
            String iin = generatorIIN.generate();
            if (CheckIIN.checkIIN(iin)) {
                System.out.println(iin);
                i++;
            }
        }
    }
}

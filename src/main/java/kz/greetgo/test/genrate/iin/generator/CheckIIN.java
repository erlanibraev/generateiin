package kz.greetgo.test.genrate.iin.generator;

/**
 * Created by Erlan Ibraev on 31.03.2017.
 */
public class CheckIIN {

    public static boolean checkIIN(String iin) {
        if (iin.length() != 12) {
            return false;
        }
        int sum = 0;
        for(int i=0; i< 11; i++) {
            sum += (i+1) * Integer.parseInt(String.valueOf(iin.charAt(i)));
        }
        int k = sum % 11;
        if(k == 10) {
            sum = 0;
            for(int i=0; i<11; i++) {
                int t = (i + 3) % 11;
                if(t==0) {
                    t=11;
                }
                sum += t * Integer.parseInt(String.valueOf(iin.charAt(i)));
            }
            k = sum % 11;
            if (k==10) {
                return false;
            }
            return k == Integer.parseInt(String.valueOf(iin.charAt(11)));
        }
        return k == Integer.parseInt(String.valueOf(iin.charAt(11)));
    }
}

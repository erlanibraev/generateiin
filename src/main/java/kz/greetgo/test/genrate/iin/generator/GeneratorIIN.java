package kz.greetgo.test.genrate.iin.generator;

/**
 * Created by Erlan Ibraev on 31.03.2017.
 */
public class GeneratorIIN {

    private static int MAX_YEAR = 99;
    private static int MIN_YEAR = 80;
    private static int MAX_MONTH = 12;
    private static int MAX_DAY = 27;

    public String generate() {
        StringBuilder result = new StringBuilder();
        result.append(makeYear());
        result.append(makeMonth());
        result.append(makeDay());
        result.append(makeSex());
        result.append(makeRnd());
        String str = result.toString();
        result.append(makeCRC(str));
        return result.toString();
    }

    private int makeCRC(String iin) {
        int result = 0;
        int sum = 0;
        for(int i=0; i< 11; i++) {
            sum += (i+1) * Integer.parseInt(String.valueOf(iin.charAt(i)));
        }
        int k = sum % 11;
        if(k == 10) {
            sum = 0;
            for(int i=0; i<11; i++) {
                int t = (i + 3) % 11;
                if(t==0) {
                    t=11;
                }
                sum += t * Integer.parseInt(String.valueOf(iin.charAt(i)));
            }
            k = sum % 11;
        }
        result = k;
        return result;
    }

    private String makeRnd() {
        StringBuilder result = new StringBuilder();
        for(int i=0; i < 4; i++) {
            result.append(Math.round(Math.random()*10));
        }
        return result.toString();
    }

    private int makeSex() {
        if (Math.round(Math.random()) == 1 ) {
            return 4;
        } else {
            return 3;
        }
    }

    private String makeDay() {
        int day = new Long(Math.round(Math.random()*(MAX_DAY - 1)+1)).intValue();
        return day < 10 ? "0"+day : "" + day;
    }

    private String makeMonth() {
        int month = new Long(Math.round(Math.random()*(MAX_MONTH - 1)+1)).intValue();
        return month < 10 ? "0"+month : ""+month;
    }

    private int makeYear() {
        int year = new Long(Math.round(Math.random()*(MAX_YEAR - MIN_YEAR)+MIN_YEAR)).intValue();
        return year;
    }
}

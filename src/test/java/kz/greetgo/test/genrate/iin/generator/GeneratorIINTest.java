package kz.greetgo.test.genrate.iin.generator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Erlan Ibraev on 31.03.2017.
 */
public class GeneratorIINTest {

    private GeneratorIIN generatorIIN = new GeneratorIIN();

    @Test
    public void test() {
        String iin = generatorIIN.generate();
        System.out.println(iin);
        assertTrue(CheckIIN.checkIIN(iin));
    }
}
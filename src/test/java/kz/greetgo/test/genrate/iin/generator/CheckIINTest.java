package kz.greetgo.test.genrate.iin.generator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Erlan Ibraev on 31.03.2017.
 */
public class CheckIINTest {

    public static String IIN="750617300347";

    @Test
    public void testIIN() {
        assertTrue(CheckIIN.checkIIN(IIN));
    }

    @Test
    public void testWrongLength() {
        assertFalse(CheckIIN.checkIIN("1234567890123"));
        assertFalse(CheckIIN.checkIIN("1234567890"));
    }

    @Test
    public void testNotIIN() {
        assertFalse(CheckIIN.checkIIN("610428400423"));
    }
}